;;; GNU Guix DHCP Client.
;;;
;;; Copyright � 2015 Rohan Prinja <rohan.prinja@gmail.com>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (dhcp options names)
  #:use-module (rnrs enums)
  #:export (map-name-to-code))

; For the options marked UNUSED, refer to RFC 3679.
(define *option-names*
  (make-enumeration '(PAD
		      TIME-OFFSET
		      SUBNET-MASK
		      ROUTER
		      TIME-SERVER
		      NAME-SERVER
		      DOMAIN-NAME-SERVER
		      LOG-SERVER
		      QUOTE-SERVER
		      LPR-SERVER
		      IMPRESS-SERVER
		      RESOURCE-LOCATION-SERVER
		      HOST-NAME
		      BOOT-FILE-SIZE
		      MERIT-DUMP-FILE
		      DOMAIN-NAME
		      SWAP-SERVER
		      ROOT-PATH
		      EXTENSIONS-PATH
		      IP-FORWARDING-ENABLE/DISABLE
		      NON-LOCAL-SOURCE-ROUTING-ENABLE/DISABLE
		      POLICY-FILTER
		      MAXIMUM-DATAGRAM-REASSEMBLY-SIZE
		      DEFAULT-IP-TIME-TO-LIVE
		      PATH-MTU-AGING-TIMEOUT
		      PATH-MTU-PLATEAU-TABLE
		      INTERFACE-MTU
		      ALL-SUBNETS-ARE-LOCAL
		      BROADCAST-ADDRESS
		      PERFORM-MASK-DISCOVERY
		      MASK-SUPPLIER
		      PERFORM-ROUTER-DISCOVERY
		      ROUTER-SOLICITATION-ADDRESS
		      STATIC-ROUTING-TABLE
		      TRAILER-ENCAPSULATION
		      ARP-CACHE-TIMEOUT
		      ETHERNET-ENCAPSULATION
		      DEFAULT-TCP-TTL
		      TCP-KEEPALIVE-INTERVAL
		      TCP-KEEPALIVE-GARBAGE
		      NETWORK-INFORMATION-SERVICE-DOMAIN
		      NETWORK-INFORMATION-SERVERS
		      NTP-SERVERS
		      VENDOR-SPECIFIC-INFORMATION
		      NETBIOS-OVER-TCP/IP-NAME-SERVER
		      NETBIOS-OVER-TCP/IP-DATAGRAM-DISTRIBUTION-SERVER
		      NETBIOS-OVER-TCP/IP-NODE-TYPE
		      NETBIOS-OVER-TCP/IP-SCOPE
		      X-WINDOW-SYSTEM-FONT-SERVER
		      X-WINDOW-SYSTEM-DISPLAY-MANAGER
		      REQUESTED-IP-ADDRESS
		      IP-ADDRESS-LEASE-TIME
		      OPTION-OVERLOAD
		      DHCP-MESSAGE-TYPE
		      SERVER-IDENTIFIER
		      PARAMETER-REQUEST-LIST
		      MESSAGE
		      MAXIMUM-DHCP-MESSAGE-SIZE
		      RENEW-TIME-VALUE
		      REBINDING-TIME-VALUE
		      CLASS-IDENTIFIER
		      CLIENT-IDENTIFIER
		      NETWARE/IP-DOMAIN-NAME
		      NETWARE/IP-INFORMATION
		      NETWORK-INFORMATION-SERVICE+-DOMAIN
		      NETWORK-INFORMATION-SERVICE+-SERVERS
		      TFTP-SERVER-NAME
		      BOOTFILE-NAME
		      MOBILE-IP-HOME-AGENT
		      SIMPLE-MAIL-TRANSPORT-PROTOCOL-SERVER
		      POST-OFFICE-PROTOCOL-SERVER
		      NETWORK-NEWS-TRANSPORT-PROTOCOL-SERVER
		      DEFAULT-WORLD-WIDE-WEB-SERVER
		      DEFAULT-FINGER-SERVER
		      DEFAULT-INTERNET-RELAY-CHAT-SERVER
		      STREETTALK-SERVER
		      STREETTALK-DIRECTORY-ASSISTANCE-SERVER
		      USER-CLASS-INFORMATION
		      SLP-DIRECTORY-AGENT
		      SLP-SERVICE-SCOPE
		      RAPID-COMMIT
		      FULLY-QUALIFIED-DOMAIN-NAME
		      RELAY-AGENT-INFORMATION
		      INTERNET-STORAGE-NAME-SERVICE
		      UNUSED
		      NDS-SERVERS
		      NDS-TREE-NAME
		      NDS-CONTEXT
		      BCMCS-CONTROLLER-DOMAIN-NAME-LIST
		      BCMCS-CONTROLLER-IPV4-ADDRESS-LIST
		      AUTHENTICATION
		      CLIENT-LAST-TRANSACTION-TIME
		      ASSOCIATED-IP
		      CLIENT-SYSTEM-ARCHITECTURE-TYPE
		      CLIENT-NETWORK-INTERFACE-IDENTIFIER
		      LIGHTWEIGHT-DIRECTORY-ACCESS-PROTOCOL
		      UNUSED
		      CLIENT-MACHINE-IDENTIFIER
		      OPEN-GROUP'S-USER-AUTHENTICATION
		      GEOCONF-CIVIC
		      IEEE-1003.1-TZ-STRING
		      REFERENCE-TO-THE-TZ-DATABASE
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      NETINFO-PARENT-SERVER-ADDRESS
		      NETINFO-PARENT-SERVER-TAG
		      URL
		      UNUSED
		      AUTO-CONFIGURE
		      NAME-SERVICE-SEARCH
		      SUBNET-SELECTION
		      VARIABLEDNS-DOMAIN-SEARCH-LIST
		      VARIABLESIP-SERVERS-DHCP-OPTION
		      CLASSLESS-STATIC-ROUTE-OPTION
		      VARIABLECCC,-CABLELABS-CLIENT-CONFIGURATION
		      GEOCONF
		      VENDOR-IDENTIFYING-VENDOR-CLASS
		      VENDOR-IDENTIFYING-VENDOR-SPECIFIC
		      UNUSED
		      UNUSED
		      TFTP-SERVER-IP-ADDRESS
		      CALL-SERVER-IP-ADDRESS
		      DISCRIMINATION-STRING
		      REMOTE-STATISTICS-SERVER-IP-ADDRESS
		      802.1P-VLAN-ID
		      802.1Q-L2-PRIORITY
		      DIFFSERV-CODE-POINT
		      HTTP-PROXY-FOR-PHONE-SPECIFIC-APPLICATIONS
		      AUTHENTICATION-AGENT
		      LOST-SERVER
		      CAPWAP-ACCESS-CONTROLLER-ADDRESSES
		      OPTION-IPV4-ADDRESS-MOS
		      OPTION-IPV4-FQDN-MOS
		      SIP-UA-CONFIGURATION-SERVICE-DOMAINS
		      OPTION-IPV4-ADDRESS-ANDSF
		      OPTION-IPV6-ADDRESS-ANDSF
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      TFTP-SERVER-ADDRESS/ETHERBOOT.GRUB-CONFIGURATION-PATH-NAME
		      STATUS-CODE
		      BASE-TIME
		      START-TIME-OF-STATE
		      QUERY-START-TIME
		      QUERY-END-TIME
		      DHCP-STATE
		      DATA-SOURCE
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      ETHERBOOT
		      IP-TELEPHONE
		      ETHERBOOT.-PACKETCABLE-AND-CABLEHOME
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      PXELINUX.MAGIC-(STRING)-=-F1:00:74:7E-(241.0.116.126)
		      PXELINUX.CONFIGFILE-(TEXT)
		      PXELINUX.PATHPREFIX-(TEXT)
		      PXELINUX.REBOOTTIME-(UNSIGNED-INTEGER-32-BITS)
		      OPTION-6RD
		      OPTION-V4-ACCESS-DOMAIN
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      UNUSED
		      SUBNET-ALLOCATION
		      PRIVATE-USE
		      UNUSED
		      UNUSED
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      PRIVATE-USE
		      END)))

; Credit: the above list of option names was obtained by using
; regexes to clean up the HTML source of this page:
; http://www.networksorcery.com/enp/protocol/bootp/options.htm

; Map a DHCP option name to its option code,
; which lies between 0 and 255 inclusive.
(define-syntax-rule (map-name-to-code name)
  ((enum-set-indexer *option-names*) name))
