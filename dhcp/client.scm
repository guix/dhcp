;;; GNU Guix DHCP Client.
;;;
;;; Copyright � 2015 Rohan Prinja <rohan.prinja@gmail.com>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/local/bin/guile \
-e main -s
coding: utf-8
!#

;; DHCP client module
(define-module (dhcp client)
  #:use-module (dhcp dhcp)
  #:use-module (ice-9 getopt-long)
  #:use-module (dhcp messages)
  #:export (main))

(define *help-message* "\
dhcp-client [options]
  -v, --version    Display version
  -h, --help       Display this help
")

(define (main args)
  "Process command-line arguments and run the client"
  (let* ((option-spec '((version (value #f))
                        (help (single-char #\h))
			(four (single-char #\4) (value #t))
			(six (single-char #\6))
			(verbose)
			(release)
			(foreground (single-char #\d))
			(continue-running (single-char #\w))
			(no-wait)
			(stop (single-char #\x))
			(port (single-char #\p) (value #t))
			(server-addr (single-char #\s) (value #t))))
         (options (getopt-long args option-spec))
         (help-wanted (option-ref options 'help #f))
         (version-wanted (option-ref options 'version #f)))
    (if (or version-wanted help-wanted)
        (begin
          (if version-wanted
              (display "dhcp-client version 0.1\n"))
          (if help-wanted
              (display *help-message*)))
        (begin
          (display "Hello, World!") (newline)))))

;; Seed the random state.
(set! *random-state* (random-state-from-platform))
