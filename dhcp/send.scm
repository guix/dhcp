;;; GNU Guix DHCP Client.
;;;
;;; Copyright � 2015 Rohan Prinja <rohan.prinja@gmail.com>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (dhcp send)
  #:use-module (rnrs bytevectors)
  #:export (make-dgram-sock
	    make-broadcast-sockaddr
	    *client-in-port*
	    *client-out-port*))

(define-syntax-rule (make-dgram-sock)
  (socket PF_INET SOCK_DGRAM IPPROTO_UDP))

; Constants for the DHCP protocol.
(define *client-out-port* 67)
(define *client-in-port* 68)

(define-syntax-rule (make-broadcast-sockaddr port)
  (make-socket-address AF_INET
		       INADDR_BROADCAST
		       port))
