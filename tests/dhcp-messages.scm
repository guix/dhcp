;;; GNU Guix DHCP Client.
;;;
;;; Copyright � 2015 Rohan Prinja <rohan.prinja@gmail.com>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (test-dhcp-messages))

(use-modules (srfi srfi-64)
	     ((guix build syscalls) #:select (all-network-interfaces))
	     (dhcp messages)
	     (dhcp interfaces)
	     (dhcp dhcp)
	     (dhcp options base)
	     (dhcp options names)
	     (rnrs bytevectors))

(define netif
  (make-network-interface
   (car
    (retain-ethernet-interfaces
     (all-network-interfaces)))
   'DHCP-INIT))


(define msg-type-code (map-name-to-code
			 'DHCP-MESSAGE-TYPE))

(define original
  (make-dhcpdiscover netif (make-vector 256 #f)))

(display (vector-ref (dhcp-msg-options original) 243))
(newline) (newline)

(test-begin "dhcp-messages")

(test-eq "message-type"
	 (map-type-to-code 'DHCPDISCOVER)
	 (bytevector-u8-ref (msg-type original) 0))

(define serialized (serialize-dhcp-msg original))

;(define opts (deserialize-options serialized 240))

(define deserialized (deserialize-dhcp-msg serialized))

(test-eqv "fields-preserved-xid"
	  (dhcp-msg-xid deserialized)
	  (dhcp-msg-xid original))

(test-equal "fields-preserved-options"
	    (dhcp-msg-options deserialized)
	    (dhcp-msg-options original))

(test-end)

(exit (zero? (test-runner-fail-count (test-runner-current))))
