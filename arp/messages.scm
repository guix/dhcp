#!/usr/local/bin/guile
coding: utf-8
!#

; Tell Guile to also search for modules in the
; current directory.
(add-to-load-path (dirname (current-filename)))

; Module for constructing and parsing ARP messages
(define-module (arp-messages)
  #:export (<arp-message>))

(use-modules (oop goops))

(define-class <arp-message> ()
  (hw-type #:init-value 1) ; ethernet
  (protocol-type #:init-value #x800)
  (hlen #:init-value 48)
  (plen #:init-value 32) ; ipv4
  'TODO)
