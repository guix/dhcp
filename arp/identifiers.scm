;;; GNU Guix DHCP Client.
;;;
;;; Copyright 2015 Free Software Foundation, Inc.
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

; ARP protocol HARDWARE identifiers.
; Taken from <linux/if_arp.h>
(define-module (arp identifiers)
  #:export (ARPHRD_NETROM
	    ARPHRD_ETHER
	    ARPHRD_EETHER
	    ARPHRD_AX25
	    ARPHRD_PRONET
	    ARPHRD_CHAOS
	    ARPHRD_IEEE802
	    ARPHRD_ARCNET
	    ARPHRD_APPLETLK
	    ARPHRD_DLCI
	    ARPHRD_ATM
	    ARPHRD_METRICOM
	    ARPHRD_IEEE1394
	    ARPHRD_EUI64
	    ARPHRD_INFINIBAND

	    map-code-to-ident))

(use-modules (rnrs enums)
	     ((srfi srfi-1) #:select (find)))

; Define IDENT to have value VAL on GNU/Linux systems,
; and -1 elsewhere (for now).
(define-syntax-rule (identifier-value ident val)
  (define ident
    (if (string-contains %host-type "linux") val -1)))

(identifier-value ARPHRD_NETROM 0)
(identifier-value ARPHRD_ETHER 1)
(identifier-value ARPHRD_EETHER 2)
(identifier-value ARPHRD_AX25 3)
(identifier-value ARPHRD_PRONET 4)
(identifier-value ARPHRD_CHAOS 5)
(identifier-value ARPHRD_IEEE802 6)
(identifier-value ARPHRD_ARCNET 7)
(identifier-value ARPHRD_APPLETLK 8)
(identifier-value ARPHRD_DLCI 15)
(identifier-value ARPHRD_ATM 19)
(identifier-value ARPHRD_METRICOM 23)
(identifier-value ARPHRD_IEEE1394 24)
(identifier-value ARPHRD_EUI64 27)
(identifier-value ARPHRD_INFINIBAND 32)

(define *arp-hardware-identifiers*
  '((ARPHRD_NETROM . 0)
    (ARPHRD_ETHER . 1)
    (ARPHRD_EETHER . 2)
    (ARPHRD_AX25 . 3)
    (ARPHRD_PRONET . 4)
    (ARPHRD_CHAOS . 5)
    (ARPHRD_IEEE802 . 6)
    (ARPHRD_ARCNET . 7)
    (ARPHRD_APPLETLK . 8)
    (ARPHRD_DLCI . 15)
    (ARPHRD_ATM . 19)
    (ARPHRD_METRICOM . 23)
    (ARPHRD_IEEE1394 . 24)
    (ARPHRD_EUI64 . 27)
    (ARPHRD_INFINIBAND . 32)))

; Given an integer CODE return the corresponding ARP
; hardware types as a symbol. This is needed because
; there are "gaps", for example there is no ARP h/w
; type for each of the integer values 9 through 14.
(define (map-code-to-ident code)
  (car (find (lambda (pair) (= (cdr pair) code))
	     *arp-hardware-identifiers*)))
